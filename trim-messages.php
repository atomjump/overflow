<?php

	/*
	
	Trim message forums that are flagged to be trimmed from the oldest first. 
	This will be called hourly on a regular cronjob.
	
	0 * * * *       /usr/bin/php /your_server_path/api/plugins/overflow/trim-messages.php
	
	1. Loop through each message from the bottom of that forum and search individually within that message for
	images that are held on this server. They will start with the config.json JSON entry uploads.vendor.imageURL
	if images are uploaded via AmazonAWS (/digitalocean), or with the current server URL /images/im/ if uploaded
	to the same server.
	2. Delete the image from using the AmazonAWS API or the local file-system unlink();
	3. Delete the message
	4. Repeat for all messages in the list (at the bottom of the forum). 
	
	You may want to run this on a lower priority e.g.
	
	0 * * * *      /usr/bin/nice -n 30 /usr/bin/php -q /var/www/html/atomjump/api/plugins/overflow/trim-messages.php
	
	
	Warning: manually running this script can pause unexpectedly at the start.  The length of time is configured by the config option
	"randomPause" in seconds.
	
	
	TODO: archiving old messages
	
	*/

	$preview = false;		//Usually set to false, unless we are testing this manually 

	use Aws\S3\S3Client;
	use Aws\S3\Exception\S3Exception;
	
	require('vendor/aws-autoloader.php');


	function trim_trailing_slash_local($str) {
        return rtrim($str, "/");
    }
    
    function add_trailing_slash_local($str) {
        //Remove and then add
        return rtrim($str, "/") . '/';
    }
    
    function was_created_on_group($api, $image_file, $image_folder, $this_layer) {
    	//Returns true for on this group, or false for not on this group
    	
     	$sql = "SELECT int_layer_id FROM tbl_media WHERE var_filename = ?";
    	$result_msgs = $api->db_select($sql, [["s", $image_file]]);
		if($row = $api->db_fetch_array($result_msgs))
		{
			if($row['int_layer_id'] == $this_layer) {
				//Yes, the image was created on this layer
				return true;
			}
		
		}
		
		return false;
    }
    
    function delete_media_entry($api, $image_file) {
    	//Also fully delete the media entry in the database, so that we will not grow disk space continuously (note: we can up to a max 1M folders created on a disk /0/1/2/3/4/5
    	//10 to the power of 6 = 1M, but at least that has a maximum - I don't think we need to delete these individually as we go? Although monitor the size of the drive used by these). The answer would be to delete these in this function if the folder was empty.
    	$sql = "DELETE FROM tbl_media WHERE var_filename = ?";
    	$result = $api->db_select($sql, [["s", $image_file]]);
    	echo "Deleted media table entry " . $image_file . "\n";
    	return;
    }
    
    
	function delete_image($image_file, $image_folder, $preview = false) {
		global $local_server_path;
		global $cnf;
		global $overflow_config;
		
		

		if(isset($cnf['uploads']['use'])) {
			if($cnf['uploads']['use'] == "amazonAWS") {

				if(isset($cnf['uploads']['vendor']['amazonAWS']['uploadUseSSL'])) {
					$use_ssl = $cnf['uploads']['vendor']['amazonAWS']['uploadUseSSL'];
					
				} else {
					$use_ssl = false;		//Default
				}
				
				if(isset($cnf['uploads']['vendor']['amazonAWS']['uploadEndPoint'])) {
					$endpoint = $cnf['uploads']['vendor']['amazonAWS']['uploadEndPoint'];
				} else {
					$endpoint = "https://s3.amazonaws.com";		//Default
				}
				
				if(isset($cnf['uploads']['vendor']['amazonAWS']['bucket'])) {
					$bucket = $cnf['uploads']['vendor']['amazonAWS']['bucket'];
				} else {
					$bucket = "ajmp";		//Default
				}
				
				if(isset($cnf['uploads']['vendor']['amazonAWS']['region'])) {
					$region = $cnf['uploads']['vendor']['amazonAWS']['region'];
				} else {
					$region = 'nyc3';
				}		
		
				
				
				$output = "Preparing to delete image: " . $image_file . "    from bucket: " . $bucket .   "   from region: " . $region .  "   at endpoint: " . $endpoint;
				echo $output . "\n";
				error_log($output);
				
				
				if($preview !== false) {
					//A preview, always return deleted
					return true;
				} else {
		
					echo "S3 connection about to be made\n";
										
					//Get an S3 client
					$s3 = new Aws\S3\S3Client([
							'version' => 'latest',
							'region'  => $region,				
							'endpoint' => $endpoint,			//E.g. 'https://nyc3.digitaloceanspaces.com'
							'credentials' => [
									'key'    => $cnf['uploads']['vendor']['amazonAWS']['accessKey'],
									'secret' => $cnf['uploads']['vendor']['amazonAWS']['secretKey'],
								]
					]);
					
					
		
					if($s3 != false) {
						echo "S3 connection made\n";
						
						try {
							// Upload data.
							$s3->deleteObject([
								'Bucket' => $bucket,
								'Key'    => $image_file
							]);

							// Print the URL to the object.
							error_log("Successfully deleted: " . $image_file);
							echo "Successfully deleted: " . $image_file . "\n";
							//Deleted correctly
						
							return true;
						} catch (S3Exception $e) {
							//Error deleting from Amazon
							error_log($e->getMessage());
							echo "Error deleting: " . $e->getMessage() . "\n";
							return false;
						}
					} else {
						echo "S3 connection not made\n";
						return false;
					}
				} 
			} else {
			
				//Delete locally
				$output = "Preparing to delete image: " . $image_folder . $image_file;
				
				
			
				echo $output . "\n";
				error_log($output);
				if($preview !== true) {
					$file_to_delete = $image_folder . $image_file;
					if(file_exists($file_to_delete)) {
						if(unlink($file_to_delete)) {
							echo "Success deleting " . $file_to_delete . "\n";
							error_log("Success deleting ". $file_to_delete . "  [error log version]");
						} else {
							echo "Failure deleting." . $file_to_delete . "\n";
							error_log("Failure deleting ". $file_to_delete . "  [error log version]");
						}
					} else {
						echo "Note: File has already been deleted: " . $file_to_delete . ".\n";
						error_log("Note: File has already been deleted: " . $file_to_delete . "  [error log version]");
					}
				} 
				
				//Now do this for each of the $cnf['ips'], call a delete_image script on each 
				if(isset($overflow_config['urlPathToDeleteScript'])) {
					for($cnt = 0; $cnt < count($cnf['ips']); $cnt++) {
						$url = "http://" . $cnf['ips'][$cnt] . $overflow_config['urlPathToDeleteScript'] . "?code=" . $overflow_config['securityCode'] . "&imageName=" . $image_file;
						echo "Running URL " . $url . " to delete image.\n";
						if($preview !== true) {
							file_get_contents($url);
						}
					}
				}
				
				return true;
				
			}
		}

		
		
	}    
    
    
    function trim_messages($api, $sql, $params, $fully_delete, $preview, $notify, $image_folder, $blur = false, $this_layer) {
    		
    	//Trims a list of messages with a SQL command to define which ones.
    	//Returns the message id of the last message processed (or null if there were none).
		
		//Note: this function could take quite a while, potentially longer than the gap between CRON requests, so it could be
		//re-entrant. For this reason, we have an 'enm_lockon' that
		
		
		$first_msg_id = null;
		$last_blurred_msg_id = null;
		
		$result_msgs = $api->db_select($sql, $params);
		while($row_msg = $api->db_fetch_array($result_msgs))
		{
			echo "Message: " . $row_msg['var_shouted'] . "    ID:" . $row_msg['int_ssshout_id'] . "\n";
			
			if(!$first_msg_id) $first_msg_id = $row_msg['int_ssshout_id'];		//Store for the return value
			
			global $cnf;
			
			if($fully_delete === true) {
				
				
				//Search for any images in the message
				echo "Search term = " . $cnf['uploads']['replaceHiResURLMatch'] . "\n";
				$url_matching = "ajmp";		//Works with Amazon based jpgs on atomjump.com which include ajmp.
				if($cnf['uploads']['replaceHiResURLMatch']) $url_matching = $cnf['uploads']['replaceHiResURLMatch'];
				
				
				$ext_types = array(".jpg", ".mp3", ".mp4", ".pdf");
				for($cnt_ext = 0; $cnt_ext < count($ext_types); $cnt_ext ++) {
				
					$ext = $ext_types[$cnt_ext];
				
					$preg_search = "/.*?" . $url_matching ."(.*?)\\" . $ext . "/i";
					preg_match_all($preg_search, $row_msg['var_shouted_processed'], $matches);			//Need to match against the full processed message - it may include a 
																										//thumbnail .jpg and .mp4, for example for videos.
				
					
						
					if(count($matches) > 1) {
						//Yes we have at least one image
						for($cnt = 0; $cnt < count($matches[1]); $cnt++) {
							echo "Matched image raw: " . $matches[1][$cnt] . "\n";
							list($end_of_url, $raw_image_name) = explode( "/images/im/", $matches[1][$cnt]);
							
							$image_name = $raw_image_name . $ext;
							if($ext == ".jpg") {
								$image_hi_name = $raw_image_name . "_HI.jpg";
							} else {
								$image_hi_name = null;
							}
							echo "Image name: " . $image_name . "\n";
							
							//Check if this image was created from this group
							$from_this_group = was_created_on_group($api, $image_name, $image_folder, $this_layer);
							echo "From this group: " . $from_this_group . "\n";	
							
							if($blur == false) {
								//Delete the low-res image (as well as the hi-res one, below)
								if($from_this_group == true) {
									delete_image($image_name, $image_folder, $preview);
									delete_media_entry($api, $image_name);		//Since we're deleting the core entry, and not just the hi-res version, we also need
																				//to remove the database version of this - note: once this has gone we have no way to trace it 
																				//again.
								}
							} else {						
								//In a blur situation, here, we delete the hi-res image only, and leave the normal 'blurred' low-res version.	
							}
							
							if($ext == ".jpg") {
								//If we're dealing with an image, remove the hi-res version
								if($from_this_group == true) {
									delete_image($image_hi_name, $image_folder, $preview);
								}
							
								//And link to the low-res version
								if($from_this_group == true) {
									if($blur == true) {
										$last_blurred_msg_id = $row_msg['int_ssshout_id'];		//Record for next time
									
										//We want to replace [filename]_HI.jpg versions in the message itself with [filename].jpg, since those
										//versions no longer exist.
										$old_shouted = $row_msg['var_shouted_processed'];
										echo "Replacing _HI versions within the processed version of the message: " . $old_shouted ."\n";
										$new_shouted = str_replace("_HI.jpg", ".jpg", $old_shouted);
										echo "New message being entered: " . clean_data_keep_tags($new_shouted) . "\n";
										$api->db_update("tbl_ssshout", "var_shouted_processed = '" . clean_data_keep_tags($new_shouted) ."' WHERE int_ssshout_id = " . $row_msg['int_ssshout_id']);
									}	
								}
							}
							
						}
					}
				}
				
				
				
				
				if($blur !== true) {
					//Delete the record
					if(($preview == false)) {
					
						echo "Deleting message " . $row_msg['int_ssshout_id'] . "\n";
						error_log("Deleting message " . $row_msg['int_ssshout_id']);
						$sql_del = "DELETE FROM tbl_ssshout WHERE int_ssshout_id = ?";
						$api->db_select($sql_del, [["i", $row_msg['int_ssshout_id']]]);
					} else {
						echo "Would be deleting message " . $row_msg['int_ssshout_id'] . "\n";
					
					}
				}
				
			
			
			} else {
				//Not deleting the image from the disk, but deactivating the message
				if($blur == true) {
					echo "Blurring image.";
				} else {
					echo "Deactivating. But leaving images.";
					if($preview == false) {
					   echo "Deactivating message " . $row_msg['int_ssshout_id'] . "\n";
					   error_log("Deactivating message " . $row_msg['int_ssshout_id']);
					   
					   $api->db_update("tbl_ssshout", "enm_active = 'false' WHERE int_ssshout_id = " . $row_msg['int_ssshout_id']);
					} else {
						echo "Would be deactivating message " . $row_msg['int_ssshout_id'] . "\n";
					}
				}
			}
		}
	
		if($blur == true) {
			return $last_blurred_msg_id;
		} else {			
			return $first_msg_id;
		}
	}
    


	if(!isset($overflow_config)) {
        //Get global plugin config - but only once
		$data = file_get_contents (dirname(__FILE__) . "/config/config.json");
        if($data) {
            $overflow_config = json_decode($data, true);
            if(!isset($overflow_config)) {
                echo "Error: overflow config/config.json is not valid JSON.";
                exit(0);
            }
     
        } else {
            echo "Error: Missing config/config.json in overflow plugin.";
            exit(0);
     
        }
  
  
    }

	//Warning: the script will silently pause for up to the number of seconds in 

    $agent = $overflow_config['agent'];
	ini_set("user_agent",$agent);
	$_SERVER['HTTP_USER_AGENT'] = $agent;
	$start_path = add_trailing_slash_local($overflow_config['serverPath']);
	if($overflow_config['layerTitleDbOverride']) {
		//Override the selected database
		$_REQUEST['uniqueFeedbackId'] = $overflow_config['layerTitleDbOverride'];
	}
	if($overflow_config['preview']) {
		$preview = $overflow_config['preview'];
	}
	
	if($overflow_config['randomPause']) {
		//Pause execution for a random interval to prevent multiple servers in a cluster calling at the same time on the cron.
		$pause_by = rand(1,$overflow_config['randomPause']);
		sleep($pause_by);
	}

	$image_folder = $start_path . "images/im/";
	
	$notify = false;
	include_once($start_path . 'config/db_connect.php');	
	
	$define_classes_path = $start_path;     //This flag ensures we have access to the typical classes, before the cls.pluginapi.php is included
	require($start_path . "classes/cls.pluginapi.php");
	
	$api = new cls_plugin_api();
	
	
	if($preview == true) {
		echo "Preview mode ON\n";
	}
	
	
	echo "Using database host: " .  $db_cnf['hosts'][0] . "  name:" . $db_cnf['name'] . "\n";
		
	$fully_delete = false;		
	if(isset($db_cnf['deleteDeletes'])) {
		//Defaults to the server-defined option, unless..
		$fully_delete = $db_cnf['deleteDeletes'];
	}
	if(isset($overflow_config['fullyDelete'])) {
		//Unless we have an override in our local config
		$fully_delete = $overflow_config['fullyDelete'];	
	}
	
	echo "Fully deleting: " . $fully_delete . "\n";
	

	echo "Checking for layers due to be trimmed...\n";
	$sql = "SELECT * FROM tbl_overflow_check WHERE enm_due_trimming = 'true'";
    $result = $api->db_select($sql);
	while($row = $api->db_fetch_array($result))
	{
	
			$this_layer = $row['int_layer_id'];
			
			echo "Layer: " . $this_layer . "\n";

			if($row['enm_lockon'] == 'false') {
				//Put a lock on this forum while we're processing to prevent re-entrant calls
				$api->db_update("tbl_overflow_check", "enm_lockon = 'true' WHERE int_layer_id = " . $this_layer);


			
				//Get messages in the forum that are aged - sort by order inserted, up to the limit of the user defined overflow limit
				$old_messages_cnt = $row['int_current_msg_cnt'];
				$messages_to_trim = $old_messages_cnt - $row['int_max_messages'];		
				$max_messages_to_trim = $old_messages_cnt - $row['int_max_messages'] + 500;  //The 500 is an arbitrary number that is more than people are likely to delete in an hour
				$messages_to_not_trim = $row['int_max_messages'];			//These are the ones that should not be touched - the most recent.
				$current_trimmed_cnt = $row['int_cnt_trimmed'];		//Use this for writing back the trimmed count as a record
				$sql = "SELECT int_ssshout_id, var_shouted, var_shouted_processed FROM tbl_ssshout WHERE int_layer_id = ? AND enm_active = 'true' ORDER BY int_ssshout_id DESC LIMIT ?, ?";
				$params = [["i", $this_layer], ["i",$messages_to_not_trim], ["i", $max_messages_to_trim]];
				
				$first_msg_id = trim_messages($api, $sql, $params, $fully_delete, $preview, $notify, $image_folder, false, $this_layer);		//false is full message trimming (not blurring)
				
				if($first_msg_id) {
					//now remove the inactive messages (typically 'typing' etc.) up until the end of the last message
					$sql = "SELECT int_ssshout_id, var_shouted, var_shouted_processed FROM tbl_ssshout WHERE int_layer_id = ? AND enm_active != 'true' AND int_ssshout_id < ? ORDER BY int_ssshout_id";
					$params = [["i", $this_layer],["i",$first_msg_id]]; 
					trim_messages($api, $sql, $params, $fully_delete, $preview, $notify, $image_folder, false, $this_layer);		//false is full message trimming (not blurring)
					//Note: these messages are not counted in the trimmed count.
				}


				//Because the functions above could take a while, there is a fairly high chance that the number of messages has increased while it was running. We therefore need to get the current values again.
				$sqlb = "SELECT * FROM tbl_overflow_check WHERE int_layer_id = ?";
				$resultb = $api->db_select($sqlb, [["i", $this_layer]]);
				if($rowb = $api->db_fetch_array($resultb))
				{
					$old_messages_cnt = $rowb['int_current_msg_cnt'];
					$current_trimmed_cnt = $rowb['int_cnt_trimmed'];
				}
				
				
				$new_trimmed_cnt = $current_trimmed_cnt + $messages_to_trim;
				$new_messages_cnt = $old_messages_cnt - $messages_to_trim;
				if($preview == false) {
					//Write back the number of messages trimmed into the tbl_overflow record, reduce the count, switch to 'not due a trimming'
					$api->db_update("tbl_overflow_check", "int_current_msg_cnt = ?, int_cnt_trimmed = ?, enm_due_trimming = 'false' WHERE int_layer_id = ?", [["i", $new_messages_cnt],["i", $new_trimmed_cnt],["i", $this_layer]]);
					echo "Set message cnt = " . $new_messages_cnt . ", trimmed cnt = " . $new_trimmed_cnt . " for layer " . $this_layer . "\n";
				} else {
					echo "Would set message cnt = " . $new_messages_cnt . ", trimmed cnt = " . $new_trimmed_cnt . " for layer " . $this_layer . "\n";
				}
				
				//Remove the lock on this forum
				$api->db_update("tbl_overflow_check", "enm_lockon = 'false' WHERE int_layer_id = ?", [["i", $this_layer]]);
			} else {
				echo "Sorry, this forum is currently being processed by another trim request. Please wait until that has finished.\n";
				error_log("An attempt has been made to trim a forum that is already being trimmed. It will try again on the next cron request.");
			}
		
	} 
	
	
	if((isset($overflow_config['blurImages']))&&($overflow_config['blurImages'] == true)) {
		//Note: currently this feature is experimental.  It does delete the hi-res version of the image,
		//which will save space, but the image link goes to a blank AtomJump logo image when you click on it, rather than the low-res
		//version.
		echo "Checking for layers due to have their images blurred...\n";
		$sql = "SELECT * FROM tbl_overflow_check WHERE enm_due_blurring = 'true'";
		$result = $api->db_select($sql);
		while($row = $api->db_fetch_array($result))
		{
				$this_layer = $row['int_layer_id'];
				
				if($row['enm_lockon'] == 'false') {
					//Put a lock on this forum while we're processing to prevent re-entrant calls
					$api->db_update("tbl_overflow_check", "enm_lockon = 'true' WHERE int_layer_id = ?", [["i",$this_layer]]);

				
				
					$last_blurred_id = $row['int_last_blurred_msg_id'];
					if(!$last_blurred_id) $last_blurred_id = 0;
					
					echo "Layer: " . $this_layer . "\n";
					
					//Get messages in the forum that are aged - sort by order inserted, up to the limit of the user defined overflow limit
					$old_messages_cnt = $row['int_current_msg_cnt'];
					$message_start_to_blur = intval($row['int_max_messages'] * 0.3);		//30% down the list from the latest
					$max_messages_to_blur = $row['int_max_messages'];
					$current_trimmed_cnt = $row['int_cnt_trimmed'];		//Use this for writing back the trimmed count as a record
					$sql = "SELECT int_ssshout_id, var_shouted, var_shouted_processed FROM tbl_ssshout WHERE int_layer_id = ? AND int_ssshout_id > ? ORDER BY int_ssshout_id DESC LIMIT ?, ?";
					$params = [["i", $this_layer],["i", $last_blurred_id],["i", $message_start_to_blur],["i", $max_messages_to_blur]];
					echo $sql . "\n";	
					
					$last_blurred_msg_id = trim_messages($api, $sql, $params, $fully_delete, $preview, $notify, $image_folder, true, $this_layer);  //True is for blurring - no messages should be deleted
					if($last_blurred_msg_id) {
						$api->db_update("tbl_overflow_check", "int_last_blurred_msg_id = ?, enm_due_blurring = 'false' WHERE int_layer_id = ?", [["i", $last_blurred_msg_id],["i", $this_layer]]);
					} else {
						//But switch off the blur checking until later regardless.
						$api->db_update("tbl_overflow_check", "enm_due_blurring = 'false' WHERE int_layer_id = ?", [["i", $this_layer]]);
					}
					
					
					//Remove the lock on this forum
					$api->db_update("tbl_overflow_check", "enm_lockon = 'false' WHERE int_layer_id = ?", [["i", $this_layer]]);
				} else {
					echo "Sorry, this forum is currently being processed by another trim request. Please wait until that has finished.\n";
					error_log("An attempt has been made to trim a forum that is already being trimmed. It will try again on the next cron request.");
				}
				
		}
	}
	
	session_destroy();  //remove session




?>
